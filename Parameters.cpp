#include "Parameters.h"
#include <fstream>
#include "utils.h"


Parameters::Parameters(int argc, cstring argv[], cstring config)
{

	std::ifstream config_file(config);
	std::string line;
	while (std::getline(config_file, line))
	{
		std::vector<std::string> option = split_string(split_string(line, '#', 2)[0], '=', 2);
		for (std::string& s : option)
			s = trim_string(s);
		if (option.size() == 2 && !option[0].empty() && !option[1].empty())
			options[option[0]] = option[1];
	}

	for (int i = 0; i < argc; i++)
	{
		if (!strncmp(argv[i], "--", 2))
		{
			std::vector<std::string> option = split_string(argv[i] + 2, '=', 2);
			options[option[0]] = option.size() == 2 ? option[1] : "true";
		}
		else
			arguments.push_back(argv[i]);
	}

}
