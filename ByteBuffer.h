#pragma once
#include "utils.h"


class ByteBuffer
{

public:
	ByteBuffer() = delete;
	ByteBuffer(size_t size) : data(size), pointer(data.data()) {}
	ByteBuffer(ByteBuffer const&) = delete;
	~ByteBuffer() = default;
	ByteBuffer& operator = (ByteBuffer const&) = delete;

	template <typename T>
	operator T* () { return reinterpret_cast<T*>(data.data()); }

	int16_t ReadWord();
	void WriteWord(int16_t word);

	void Reset();

private:
	std::vector<byte> data;
	byte *pointer;

};
