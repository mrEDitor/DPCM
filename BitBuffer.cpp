#include "BitBuffer.h"

unsigned BitBuffer::ReadCodeword(unsigned bits)
{
	while (unsigned(buffer_size) < bits)
	{
		buffer_size += 8;
		buffer = buffer << 8 | *pointer & 0xFF;
		*pointer++;
	}
	buffer_size -= bits;
	return buffer >> buffer_size & (1 << bits) - 1;
}

void BitBuffer::WriteCodeword(unsigned codeword, unsigned bits)
{
	buffer = buffer << bits | codeword & (1 << bits) - 1;
	buffer_size += bits;
	Flush(7);
}

void BitBuffer::Flush(int min_size)
{
	while (buffer_size > min_size)
	{
		buffer_size -= 8;
		*pointer = buffer >> buffer_size & 0xFF;
		pointer++;
	}
}

void BitBuffer::Reset()
{
	pointer = data.data();
}
