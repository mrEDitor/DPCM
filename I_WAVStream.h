#pragma once
#include "utils.h"


class I_WAVStream
{

public:
	virtual ~I_WAVStream() = default;
	virtual unsigned GetSampleRate() const = 0;
	virtual bool HasNextBlock() = 0;
	virtual size_t ReadBlock(byte block[], size_t samples) = 0;
	virtual void WriteBlock(byte const *block, size_t samples) = 0;
	virtual void Finalize() = 0;

};