#pragma once
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string>
#include <vector>


using std::size_t;
typedef char byte;
typedef char const *cstring;

std::vector<std::string> split_string(std::string const& s, char delim, size_t max_count);
std::string trim_string(std::string const& s);
int parse_int(std::string const& s, bool binary_postfix = false);

inline size_t bytes_in_bits(size_t bits)
{
	return size_t(std::ceil(bits / 8.0));
}

inline int16_t extend_signed(int16_t value, unsigned int bits)
{
	if (value >> (bits - 1) & 1)
		return 0xFFFF << bits | value & (1 << bits) - 1;
	else
		return value & (1 << bits) - 1;
}

inline unsigned get_port(std::string const& arg)
{
	if (arg.compare(0, 4, "tcp:"))
		return 0;
	return std::stoi(arg.substr(arg.rfind(':') + 1));
}

inline std::string get_address(std::string const& arg)
{
	size_t off = arg.rfind('/') + 1;
	return arg.substr(off, arg.rfind(':') - off);
}
