DPCM audio coding tool
=====

Usage
-----
	dpcm encode (FILE|CAPTURE) [FILE|PORT]
	dpcm decode (FILE|ADDRESS) [FILE|SPEAKER]
	dpcm evaluate INPUT [COMPRESSED [OUTPUT]]
where:
- FILE, INPUT, COMPRESSED and OUTPUT are filenames;
- CAPTURE is ```capture://``` and can be used for real-time audio encoding;
- SPEAKER is ```speaker://``` and can be used for real-time audio decoding;
- PORT is port number like ```tcp:16500``` to start a tcp-server;
- ADDRESS is address-port pair like ```tcp://test.com:16500``` to connect to tcp-server.

Build
-----
- Get a clone of repository:

		git clone https://gitlab.com/mrEDitor/DPCM

- Get [Simple and Fast Multimedia Library](http://www.sfml-dev.org/):
	- Windows: download [SFML](http://www.sfml-dev.org/download/sfml/) package and place SFML folder as ```../SFML``` relative to DPCM folder;
	- Ubuntu or Debian: ```sudo apt-get install libsfml-dev```;
	- Archlinux: ```pacman -S sfml```;
	- etc.

- Configure:

		chmod +x ./configure
		./configure
		cd build

- Build:
	- UNIX-way: ```make```;
	- MSVC-way: ```start ALL_BUILD.vcxproj``` and build there;

- Take your files in ```Release``` directory.

See also
-----
- There is the [LICENSE](./LICENSE) file;
- Default config file, [dpcm.conf](./dpcm.conf);
- Repository at [GitLab.com/mrEDitor/DPCM](https://gitlab.com/mrEDitor/DPCM).
