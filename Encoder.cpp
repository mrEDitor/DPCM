#include "Encoder.h"
#include <iomanip>
#include "ByteBuffer.h"
#include "BitBuffer.h"


unsigned Encoder::Encode(double sample)
{
	double prediction_error = sample - GetDecoder().GetPredictor().Get();
	int codeword = int(prediction_error / width) & out_mask;
	decoder.Decode(codeword);
	return codeword;
}

void Encoder::EncodeStream(I_WAVStream* input, I_DPCMStream* output)
{
	Encoder encoder(output->GetBitDepth(), output->GetMemoryOrder());
	ByteBuffer input_buffer(2 * output->GetSamplesPerBlock());
	BitBuffer output_buffer(bytes_in_bits(output->GetSamplesPerBlock() * output->GetBitDepth()));
	while (input->HasNextBlock())
	{
		size_t samples_count = input->ReadBlock(input_buffer, output->GetSamplesPerBlock());
		input_buffer.Reset();
		output_buffer.Reset();
		std::vector<double> const& coefficients =
			encoder.decoder.GetPredictor().CalculateCoefficients(input_buffer, samples_count);
		for (size_t i = 0; i < samples_count; i++)
		{
			output_buffer.WriteCodeword(encoder.Encode(input_buffer.ReadWord()), output->GetBitDepth());
		}
		output_buffer.Flush();
		output->WriteBlock(reinterpret_cast<byte const*>(coefficients.data()), output_buffer, samples_count);
	}
}
