#pragma once
#include "Predictor.h"
#include "I_WAVStream.h"
#include "I_DPCMStream.h"


class Decoder
{

public:
	Decoder(unsigned bits_depth, unsigned memory_order) :
		predictor(memory_order), bits_depth(bits_depth),
		memory_order(memory_order), width(65536.0 / (1 << bits_depth)) {}
	Decoder() = delete;
	Decoder(Decoder const&) = delete;
	virtual ~Decoder() = default;
	Decoder& operator = (Decoder const&) = delete;

	Predictor& GetPredictor() { return predictor; }

	double Decode(int codeword);

	static void DecodeStream(I_DPCMStream *input, I_WAVStream *output);

private:
	Predictor predictor;
	unsigned bits_depth, memory_order;
	double width;

};
