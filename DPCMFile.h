#pragma once
#include <fstream>
#include "I_DPCMStream.h"

class DPCMFile :
	public I_DPCMStream
{

public:
	DPCMFile() = delete;
	DPCMFile(std::string const& filename);
	DPCMFile(std::string const& filename,
		unsigned bit_depth, unsigned sample_rate, unsigned memory_order, unsigned samples_per_block);
	DPCMFile(DPCMFile const&) = delete;
	virtual ~DPCMFile() {}
	DPCMFile& operator = (DPCMFile const&) = delete;

	bool HasNextBlock() override;
	size_t ReadBlock(double coeff[], byte block[]) override;
	void WriteBlock(byte const* coeff, byte const* block, unsigned samples) override;
	void Finalize() override;

	unsigned GetFileSize() const;
	unsigned GetSampleRate() const override { return header.sample_rate; }
	unsigned GetBitDepth() const override { return header.bit_depth; }
	unsigned GetMemoryOrder() const override { return header.memory_order; }
	unsigned GetSamplesCount() const { return header.samples_count; }
	unsigned GetSamplesPerBlock() const override { return header.samples_per_block; }

private:
	struct
	{
		char fourcc[4];
		uint32_t sample_rate;
		uint16_t bit_depth;
		uint16_t memory_order;
		uint32_t samples_per_block;
		uint32_t samples_count;
	} header;
	std::fstream file;
	size_t samples_written;

};
