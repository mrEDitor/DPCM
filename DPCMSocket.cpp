#include "DPCMSocket.h"
#include <stdexcept>

DPCMSocket::DPCMSocket(std::string const& address, unsigned port) :
	data_state(EMPTY), port(port)
{
	if (address == sf::IpAddress::None)
	{
		throw std::runtime_error("Invalid network address");
	}
	if (socket.connect(address, port) != sf::Socket::Done)
	{
		throw std::runtime_error("Unable to establish connection");
	}
	size_t check;
	socket.receive(reinterpret_cast<char*>(&header), sizeof(header), check);
	if (check != sizeof(header))
	{
		throw std::runtime_error("Header packet is too small");
	}
}

DPCMSocket::DPCMSocket(unsigned port,
	unsigned bit_depth, unsigned sample_rate, unsigned memory_order, unsigned samples_per_block) :
	data_state(EMPTY), port(port), listener_thread(&DPCMSocket::ListenForClient, this)
{
	listener_thread.join();
	header.sample_rate = sample_rate;
	header.bit_depth = bit_depth;
	header.memory_order = memory_order;
	header.samples_per_block = samples_per_block;
	socket.send(reinterpret_cast<byte *>(&header), sizeof(header));
}

void DPCMSocket::ListenForClient()
{
	if (listener.listen(port) != sf::Socket::Done)
	{
		throw std::runtime_error("Unable to listen port");
	}
	if (listener.accept(socket) != sf::Socket::Done)
	{
		throw std::runtime_error("Unable to accept connection");
	}
	data_state = READY;
	data_cond.notify_all();
}

bool DPCMSocket::HasNextBlock()
{
	sf::Socket::Status status = socket.receive(packet);
	if (status == sf::Socket::Error)
	{
		throw std::runtime_error("Unable to send packet");
	}
	return status == sf::Socket::Done;
}

size_t DPCMSocket::ReadBlock(double coeff[], byte block[])
{
	byte const* data = reinterpret_cast<byte const*>(packet.getData());
	size_t coeff_size = sizeof(double) * header.memory_order;
	size_t block_size = packet.getDataSize() - coeff_size;
	std::copy(data, data + coeff_size, reinterpret_cast<byte*>(coeff));
	std::copy(data + coeff_size, data + packet.getDataSize(), block);
	return std::min(size_t(header.samples_per_block), size_t(block_size * 8 / header.bit_depth));
}

void DPCMSocket::WriteBlock(byte const* coeff, byte const* block, unsigned samples)
{
	std::unique_lock<std::mutex> lock(data_mutex);
	while (data_state == EMPTY)
		data_cond.wait(lock);
	sf::Packet packet;
	packet.append(coeff, sizeof(double) * header.memory_order);
	packet.append(block, bytes_in_bits(samples * header.bit_depth));
	if (socket.send(packet) != sf::Socket::Done)
	{
		throw std::runtime_error("Unable to send packet");
	}
}

