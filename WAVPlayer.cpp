#include "WAVPlayer.h"
#include <iostream>
#include <thread>

WAVPlayer::WAVPlayer(unsigned sample_rate, Mode mode) :
	data_state(mode), sample_rate(sample_rate), samples_count(0),
	control(&WAVPlayer::ControlThread, this)
{
	start_time = std::chrono::steady_clock::now();
	switch (mode)
	{
		case WAIT_READ:
			if (!isAvailable())
			{
				throw std::runtime_error("Audio capture device is unavailable");
			}
			break;
		case WAIT_WRITE:
			data.emplace_front();
			initialize(1, sample_rate);
			play();
			break;
	}
}

bool WAVPlayer::HasNextBlock()
{
	std::unique_lock<std::mutex> lock(data_mutex);
	while (data_state == WAIT_READ)
		data_cond.wait(lock);
	return data_state == READY;
}

size_t WAVPlayer::ReadBlock(byte block[], size_t samples)
{
	std::unique_lock<std::mutex> lock(data_mutex);
	size_t bytes_left = 2 * samples;
	while (bytes_left && !data.empty())
	{
		std::vector<byte>::iterator from = data.front().begin() + data_offset;
		size_t delta = std::min(bytes_left, data.front().size() - data_offset);
		std::copy(from, from + delta, block);
		bytes_left -= delta;
		if (bytes_left)
		{
			data.pop_front();
			bytes_left = 0;
		}
		else
		{
			data_offset += delta;
		}
	}
	data_state = data.empty() ? WAIT_READ : READY;
	return samples - bytes_left / 2;
}

void WAVPlayer::WriteBlock(byte const* block, size_t samples)
{
	std::unique_lock<std::mutex> lock(data_mutex);
	data.push_back(std::vector<byte>(block, block + 2 * samples));
	samples_count += samples;
	data_state = READY;
	data_cond.notify_all();
}

void WAVPlayer::Finalize()
{
	if (samples_count)
	{
		std::this_thread::sleep_until(start_time +
			std::chrono::duration<double>(double(samples_count) / double(sample_rate)));
		std::unique_lock<std::mutex> lock(data_mutex);
		data_state = FINALIZED;
		data_cond.notify_all();
	}
	if (control.joinable())
	{
		control.join();
	}
}

void WAVPlayer::ControlThread()
{
	if (data_state == WAIT_READ)
	{
		start(sample_rate);
		std::cin.ignore(64, '\n');
		sf::SoundRecorder::stop();
		std::cout
			<< std::chrono::duration<double>(std::chrono::steady_clock::now() - start_time).count()
			<< " seconds of audio seconds recorded." << std::endl;
		std::unique_lock<std::mutex> lock(data_mutex);
		data_state = FINALIZED;
		data_cond.notify_all();
	}
}

bool WAVPlayer::onGetData(Chunk& chunk)
{
	std::unique_lock<std::mutex> lock(data_mutex);
	while (data_state == WAIT_WRITE)
		data_cond.wait(lock);
	if (data_state != FINALIZED)
	{
		data.pop_front();
		chunk.samples = reinterpret_cast<int16_t*>(data.front().data());
		chunk.sampleCount = data.front().size() / 2;
		data_state = data.size() == 1 ? WAIT_WRITE : READY;
		return true;
	}
	return false;
}

bool WAVPlayer::onProcessSamples(const sf::Int16* samples, size_t sample_count)
{
	std::unique_lock<std::mutex> lock(data_mutex);
	data_cond.notify_all();
	if (data_state != FINALIZED)
	{
		byte const* new_data = reinterpret_cast<byte const*>(samples);
		data.push_back(std::vector<byte>(new_data, new_data + 2 * sample_count));
		data_state = READY;
		return true;
	}
	return false;
}
