#pragma once
#include <condition_variable>
#include <list>
#include <mutex>
#include <thread>
#include <vector>
#include <SFML/Audio.hpp>
#include "I_WAVStream.h"
#include "utils.h"

class WAVPlayer :
	private sf::SoundRecorder, private sf::SoundStream, public I_WAVStream
{

public:
	enum Mode { WAIT_READ, WAIT_WRITE, READY, FINALIZED };

	WAVPlayer(unsigned sample_rate, Mode mode);
	WAVPlayer() = delete;
	WAVPlayer(WAVPlayer const&) = delete;
	virtual ~WAVPlayer() = default;
	WAVPlayer& operator = (WAVPlayer const&) = delete;

	bool HasNextBlock() override;
	size_t ReadBlock(byte block[], size_t samples) override;
	void WriteBlock(byte const* block, size_t samples) override;
	void Finalize() override;

	void ControlThread();
	unsigned GetSampleRate() const override { return sample_rate; }

private:
	Mode data_state;
	std::mutex data_mutex;
	std::condition_variable data_cond;
	unsigned sample_rate, samples_count;
	std::list<std::vector<byte>> data;
	size_t data_offset;
	std::chrono::steady_clock::time_point start_time;
	std::thread control;

	bool onGetData(Chunk& data) override;
	bool onProcessSamples(const sf::Int16* samples, size_t sampleCount) override;
	void onSeek(sf::Time time_offset) override { /* unavailable */ }

};
