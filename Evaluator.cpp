#include "Evaluator.h"
#include <algorithm>
#include <iostream>
#include "ByteBuffer.h"
#include "I_WAVStream.h"

void Evaluator::EvaluateStream(I_WAVStream* input, I_WAVStream* output, size_t block_size)
{
	ByteBuffer input_buffer(block_size), output_buffer(block_size);
	unsigned long long signal = 0;
	unsigned long long noise = 0;
	while (input->HasNextBlock())
	{
		size_t input_size = std::min(
			input->ReadBlock(input_buffer, block_size / 2),
			output->ReadBlock(output_buffer, block_size / 2));
		input_buffer.Reset();
		output_buffer.Reset();
		for (size_t i = 0; i < input_size; i++)
		{
			int input_sample = input_buffer.ReadWord();
			int output_sample = output_buffer.ReadWord();
			signal += input_sample * input_sample;
			noise += (input_sample - output_sample) * (input_sample - output_sample);
		}
	}
	std::cout << "Signal-to-noise ratio: " << 20 * std::log10(std::sqrt(signal) / std::sqrt(noise)) << std::endl;
}
