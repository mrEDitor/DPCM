#include "WAVFile.h"

WAVFile::WAVFile(std::string const& filename) :
	file(filename, std::ios_base::in | std::ios_base::binary), samples_written(0)
{
	file.read(reinterpret_cast<char*>(&header), sizeof(header));
}

WAVFile::WAVFile(std::string const& filename, unsigned bit_depth, unsigned sample_rate) :
	file(filename, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc), samples_written(0)
{
	memcpy(header.fourcc, "RIFF", 4);
	header.chunk_size = sizeof(header) - 8;
	memcpy(header.format, "WAVE", 4);
	memcpy(header.subchunk1_id, "fmt ", 4);
	header.subchunk1_size = 16;
	header.audio_format = 1;
	header.num_channels = 1;
	header.sample_rate = sample_rate;
	header.byte_rate = 2 * sample_rate;
	header.block_align = 2;
	header.bit_depth = 16;
	memcpy(header.subchunk2_id, "data", 4);
	header.subchunk2_size = 0;
	file.write(reinterpret_cast<char*>(&header), sizeof(header));
}

void WAVFile::Finalize()
{
	if (samples_written)
	{
		header.chunk_size = 2 * samples_written + 36;
		header.subchunk2_size = 2 * samples_written;
		file.seekp(0, std::ios_base::beg);
		file.write(reinterpret_cast<char*>(&header), sizeof(header));
	}
}

bool WAVFile::HasNextBlock()
{
	return file.peek() != -1;
}

size_t WAVFile::ReadBlock(byte block[], size_t samples)
{
	file.read(block, samples * 2);
	return size_t(file.gcount() / 2);
}

void WAVFile::WriteBlock(byte const* block, size_t samples)
{
	file.write(block, samples * 2);
	samples_written += samples;
}
