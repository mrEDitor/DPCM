#pragma once
#include "utils.h"


class BitBuffer
{

public:
	BitBuffer() = delete;
	BitBuffer(size_t size) : data(size), pointer(data.data()), buffer_size(0) {}
	BitBuffer(BitBuffer const&) = delete;
	~BitBuffer() = default;
	BitBuffer& operator = (BitBuffer const&) = delete;

	operator byte* () { return data.data(); }

	unsigned ReadCodeword(unsigned bits);
	void WriteCodeword(unsigned codeword, unsigned bits);
	void Flush(int min_size = 0);

	void Reset();

private:
	std::vector<byte> data;
	byte *pointer;
	uint64_t buffer;
	int buffer_size;

};
