#include "Decoder.h"
#include "BitBuffer.h"
#include "ByteBuffer.h"
#include "utils.h"


double Decoder::Decode(int codeword)
{
	double prediction_error = extend_signed(codeword, bits_depth) * width;
	double sample = predictor.Get() + prediction_error;
	predictor.Next(sample);
	return sample;
}

void Decoder::DecodeStream(I_DPCMStream *input, I_WAVStream *output)
{
	Decoder decoder(input->GetBitDepth(), input->GetMemoryOrder());
	std::vector<double> coefficients(input->GetMemoryOrder());
	BitBuffer input_buffer(bytes_in_bits(input->GetSamplesPerBlock() * input->GetBitDepth()));
	ByteBuffer output_buffer(2 * input->GetSamplesPerBlock());
	while (input->HasNextBlock())
	{
		size_t input_size = input->ReadBlock(coefficients.data(), input_buffer);
		decoder.predictor.UseCoefficients(coefficients.data());
		input_buffer.Reset();
		output_buffer.Reset();
		for (size_t i = 0; i < input_size; i++)
		{
			output_buffer.WriteWord(int16_t(std::floor(decoder.Decode(input_buffer.ReadCodeword(input->GetBitDepth())))));
		}
		output->WriteBlock(output_buffer, input_size);
	}
}
