#include <iomanip>
#include <iostream>
#include <memory>
#include <stdexcept>
#include "DPCMFile.h"
#include "DPCMSocket.h"
#include "Encoder.h"
#include "Evaluator.h"
#include "I_WAVStream.h"
#include "I_DPCMStream.h"
#include "Parameters.h"
#include "WAVFile.h"
#include "WAVPlayer.h"
#include "utils.h"

#define CONFIG_FILE "dpcm.conf"
#define REPOSITORY_URL "http://github.com/mrEDitor/DPCM"


int main(int argc, cstring argv[])
{
	Parameters parameters(argc - 1, argv + 1, CONFIG_FILE);
	std::vector<std::string> const& arguments = parameters.GetArguments();
	std::map<std::string, std::string> const& options = parameters.GetOptions();

	try
	{
		std::cout
			<< "DPCM tool v1.0" << std::endl
			<< "-----" << std::endl;
		unsigned buffer_size = parse_int(options.at("buffer-size"), true);
		unsigned memory_order = parse_int(options.at("memory-order"));
		unsigned bit_depth = parse_int(options.at("bit-depth"));
		unsigned sample_rate = parse_int(options.at("sample-rate"));
		if (1 < arguments.size() && arguments.size() < 4 && arguments[0] == "encode")
		{
			I_WAVStream *input;
			std::string input_filename;
			if (arguments[1] == "capture://")
			{
				if (1e3 > buffer_size || buffer_size > 1e5)
				{
					std::cout << "Warning: buffer size doesn't fit in preferred size for real-time capturing" << std::endl;
				}
				input_filename = "captured";
				std::cout
					<< "Capturing audio-signal..." << std::endl
					<< "Hit [Enter] to stop." << std::endl;
				input = new WAVPlayer(sample_rate, WAVPlayer::WAIT_READ);
			}
			else
			{
				input_filename = arguments[1];
				std::cout << "Encoding file \"" << input_filename << "\"" << std::endl;
				input = new WAVFile(input_filename);
				std::cout << "("
					<< int(std::ceil(static_cast<WAVFile*>(input)->GetSamplesCount() / (buffer_size / 2.0)))
					<< " blocks * " << buffer_size / 2 << " samples * 16 bits)" << std::endl;
			}
			I_DPCMStream *output;
			if (arguments.size() == 3 && get_port(arguments[2]))
			{
				std::cout << "Send output signal over network. Waiting for a client..." << std::endl;
				output = new DPCMSocket(get_port(arguments[2]),
					bit_depth, input->GetSampleRate(), memory_order, buffer_size / 2);
				std::cout << "Connection established, client address: "
					<< static_cast<DPCMSocket*>(output)->GetRemoteAddress() << std::endl;
			}
			else
			{
				std::string output_filename = arguments.size() == 3 ? arguments[2] : input_filename + ".dpcm";
				std::cout << "Save encoded file as \"" << output_filename << "\"" << std::endl;
				output = new DPCMFile(output_filename, bit_depth, input->GetSampleRate(), memory_order, buffer_size / 2);
			}
			Encoder::EncodeStream(input, output);
			input->Finalize();
			output->Finalize();
			delete input;
			delete output;
			std::cout << "OK" << std::endl << "-----" << std::endl;
			return EXIT_SUCCESS;
		}
		if (1 < arguments.size() && arguments.size() < 4 && arguments[0] == "decode")
		{
			I_DPCMStream *input;
			std::string input_filename;
			if (get_port(arguments[1]))
			{
				std::cout << "Receive input signal over network..." << std::endl;
				input_filename = "recieved";
				input = new DPCMSocket(get_address(arguments[1]), get_port(arguments[1]));
			}
			else
			{
				input_filename = arguments[1];
				std::cout
					<< "Decode file \"" << input_filename << "\"" << std::endl;
				input = new DPCMFile(input_filename);
				std::cout << "("
					<< int(std::ceil(static_cast<DPCMFile*>(input)->GetSamplesCount() / double(input->GetSamplesPerBlock())))
					<< " blocks * " << input->GetSamplesPerBlock() << " samples * "
					<< input->GetBitDepth() << " bits)" << std::endl;
			}
			I_WAVStream *output;
			if (arguments.size() == 3 && arguments[2] == "speaker://")
			{
				if (1e3 > input->GetSamplesPerBlock() || input->GetSamplesPerBlock() > 1e5)
				{
					std::cout << "Warning: buffer size doesn't fit in preferred size for real-time playing" << std::endl;
				}
				std::cout << "Play decoded audio through speakers..." << std::endl;
				output = new WAVPlayer(input->GetSampleRate(), WAVPlayer::WAIT_WRITE);
			}
			else
			{
				std::string output_filename = arguments.size() == 3 ? arguments[2] : input_filename + ".wav";
				std::cout << "Save decoded file as \"" << output_filename << "\"" << std::endl;
				output = new WAVFile(output_filename, input->GetBitDepth(), input->GetSampleRate());
			}
			Decoder::DecodeStream(input, output);
			input->Finalize();
			output->Finalize();
			delete input;
			delete output;
			std::cout << "OK" << std::endl << "-----" << std::endl;
			return EXIT_SUCCESS;
		}
		if (1 < arguments.size() && arguments.size() < 5 && arguments[0] == "evaluate")
		{
			WAVFile *input = new WAVFile(arguments[1]);
			std::cout
				<< "Evaluating compression results of file \"" << arguments[1] << "\"" << std::endl << "("
				<< int(std::ceil(input->GetSamplesCount() / (buffer_size / 2.0)))
				<< " blocks * " << buffer_size / 2 << " samples * 16 bits = " << input->GetFileSize() << " bytes)" << std::endl;
			std::string dpcm_filename = arguments.size() > 2 ? arguments[2] : arguments[1] + ".dpcm";
			std::string output_filename = arguments.size() > 3 ? arguments[3] : dpcm_filename + ".wav";
			DPCMFile *dpcm = new DPCMFile(dpcm_filename);
			WAVFile *output = new WAVFile(output_filename);
			std::cout << "WAV/DPCM file compression ratio: "
				<< std::setprecision(5) << 100.0 * input->GetFileSize() / dpcm->GetFileSize() << "%" << std::endl;
			Evaluator::EvaluateStream(input, output, buffer_size);
			input->Finalize();
			dpcm->Finalize();
			output->Finalize();
			return EXIT_SUCCESS;
		}
	}
	catch (std::runtime_error const& exc)
	{
		std::cerr << exc.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch (std::out_of_range const&)
	{
		std::cerr << "Configuration file is incomplete!" << std::endl;
		std::cout << "-----" << std::endl
			<< "You can get correct one from:" << std::endl
			<< "\t" REPOSITORY_URL << std::endl
			<< "Copy it to '" CONFIG_FILE "'." << std::endl;
		return EXIT_FAILURE;
	}

	if (arguments.size() == 0)
	{
		std::cout
			<< "\t" "(c) 2016, Edward Minasyan <mrEDitor@mail.ru>" << std::endl
			<< "\t" "Fork it at " REPOSITORY_URL << std::endl;
	}
	std::cout
		<< "Usage:" << std::endl
		<< "\t" << argv[0] << " encode (FILE|CAPTURE) [FILE|PORT]" << std::endl
		<< "\t" << argv[0] << " decode (FILE|ADDRESS) [FILE|SPEAKER]" << std::endl
		<< "\t" << argv[0] << " evaluate INPUT [COMPRESSED [OUTPUT]]" << std::endl
		<< "where" << std::endl
		<< "\t" "FILE, INPUT, COMPRESSED and OUTPUT are filenames;" << std::endl
		<< "\t" "CAPTURE is 'capture://' and can be used for real-time audio encoding;" << std::endl
		<< "\t" "SPEAKER is 'speaker://' and can be used for real-time audio decoding;" << std::endl
		<< "\t" "PORT is port number like 'tcp:16500' to start a tcp-server;" << std::endl
		<< "\t" "ADDRESS is address-port pair like 'tcp://test.com:16500' to connect to tcp-server." << std::endl;
	return EXIT_SUCCESS;
}
