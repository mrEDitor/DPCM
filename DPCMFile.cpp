#include "DPCMFile.h"
#include <algorithm>

DPCMFile::DPCMFile(std::string const& filename) :
	file(filename, std::ios_base::in | std::ios_base::binary), samples_written(0)
{
	file.read(reinterpret_cast<char*>(&header), sizeof(header));
}

DPCMFile::DPCMFile(std::string const& filename,
	unsigned bit_depth, unsigned sample_rate, unsigned memory_order, unsigned samples_per_block) :
	file(filename, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc), samples_written(0)
{
	memcpy(header.fourcc, "DPCM", 4);
	header.sample_rate = sample_rate;
	header.bit_depth = bit_depth;
	header.memory_order = memory_order;
	header.samples_per_block = samples_per_block;
	header.samples_count = 0;
	file.write(reinterpret_cast<char*>(&header), sizeof(header));
}

bool DPCMFile::HasNextBlock()
{
	return file.peek() != -1;
}

size_t DPCMFile::ReadBlock(double coeff[], byte block[])
{
	size_t samples = bytes_in_bits(header.samples_per_block * header.bit_depth);
	if (header.memory_order)
		file.read(reinterpret_cast<char *>(coeff), header.memory_order * sizeof(double));
	file.read(block, samples);
	return std::min(size_t(header.samples_per_block), size_t(file.gcount() * 8 / header.bit_depth));
}

void DPCMFile::WriteBlock(byte const *coeff, byte const* block, unsigned samples)
{
	if (header.memory_order)
		file.write(coeff, header.memory_order * sizeof(double));
	file.write(block, bytes_in_bits(samples * header.bit_depth));
	samples_written += samples;
}

void DPCMFile::Finalize()
{
	if (samples_written)
	{
		header.samples_count = samples_written;
		file.seekp(0, std::ios_base::beg);
		file.write(reinterpret_cast<char*>(&header), sizeof(header));
	}
}

unsigned DPCMFile::GetFileSize() const
{
	return sizeof(header) + unsigned(std::ceil(header.samples_count / header.samples_per_block))
		* (sizeof(double) * header.memory_order + bytes_in_bits(header.samples_per_block * header.bit_depth));
}
