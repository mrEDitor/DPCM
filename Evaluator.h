#pragma once
#include "I_WAVStream.h"

class Evaluator
{

public:
	Evaluator() = delete;

	static void EvaluateStream(I_WAVStream *input, I_WAVStream *output, size_t block_size);

};
