#pragma once
#include <fstream>
#include "I_WAVStream.h"

class WAVFile :
	public I_WAVStream
{

public:
	WAVFile() = delete;
	WAVFile(std::string const& filename);
	WAVFile(std::string const& filename, unsigned bit_depth, unsigned sample_rate);
	WAVFile(WAVFile const&) = delete;
	virtual ~WAVFile() {}
	WAVFile& operator = (WAVFile const&) = delete;

	bool HasNextBlock() override;
	size_t ReadBlock(byte block[], size_t samples) override;
	void WriteBlock(byte const* block, size_t samples) override;
	void Finalize() override;

	unsigned GetFileSize() const { return header.chunk_size + 8; }
	unsigned GetSampleRate() const override { return header.sample_rate; }
	unsigned GetSamplesCount() const { return header.subchunk2_size / 2; }

private:
	struct
	{
		char fourcc[4];
		uint32_t chunk_size;
		char format[4];
		char subchunk1_id[4];
		uint32_t subchunk1_size;
		uint16_t audio_format;
		uint16_t num_channels;
		uint32_t sample_rate;
		uint32_t byte_rate;
		uint16_t block_align;
		uint16_t bit_depth;
		char subchunk2_id[4];
		uint32_t subchunk2_size;
	} header;
	std::fstream file;
	size_t samples_written;

};
