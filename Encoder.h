#pragma once
#include "BitBuffer.h"
#include "Decoder.h"
#include "I_WAVStream.h"
#include "I_DPCMStream.h"
#include "ByteBuffer.h"


class Encoder
{

public:
	Encoder(unsigned bits_depth, unsigned memory_order) :
		decoder(bits_depth, memory_order), bits_depth(bits_depth), memory_order(memory_order),
		out_mask((1 << bits_depth) - 1), width(65536.0 / (1 << bits_depth)) {}
	Encoder() = delete;
	Encoder(Encoder const&) = delete;
	virtual ~Encoder() = default;
	Encoder& operator = (Encoder const&) = delete;

	unsigned Encode(double sample);

	static void EncodeStream(I_WAVStream *input, I_DPCMStream *output);

	Decoder& GetDecoder() { return decoder; }

private:
	unsigned bits_depth, memory_order, out_mask;
	double width;
	Decoder decoder;

};
