#pragma once
#include <deque>
#include <vector>
#include "utils.h"

class Predictor
{

public:
	Predictor(unsigned memory_order) :
		coefficients(memory_order), memory(memory_order, 0) {}

	Predictor() = delete;
	Predictor(Predictor const&) = delete;
	virtual ~Predictor() = default;
	Predictor& operator = (Predictor const&) = delete;

	std::vector<double> const& CalculateCoefficients(int16_t block[], int size);
	void UseCoefficients(double coeffs[]);

	void Next(double actual_current);
	double Get();

private:
	std::vector<double> coefficients;
	std::deque<double> memory;

};
