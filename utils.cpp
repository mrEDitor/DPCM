#include "utils.h"
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>


std::vector<std::string> split_string(std::string const& s, char delim, size_t max_count)
{
	std::vector<std::string> r;
	std::string::size_type left_bound = 0;
	do
	{
		std::string::size_type right_bound =
			r.size() < max_count - 1 ? s.find(delim, left_bound) : std::string::npos;
		r.push_back(s.substr(left_bound, right_bound));
		left_bound = right_bound + 1;
	} while (left_bound);
	return r;
}


std::string trim_string(std::string const& s)
{
	std::string::size_type left_bound = 0;
	while (left_bound < s.size() && isspace(s[left_bound]))
		left_bound++;
	if (left_bound == s.size())
		return std::string();
	std::string::size_type right_bound = s.size() - 1;
	while (isspace(s[right_bound]))
		right_bound--;
	return s.substr(left_bound, right_bound - left_bound + 1);
}

int parse_int(std::string const& s, bool binary_postfix)
{
	int n;
	char c;
	std::stringstream buffer(s);
	buffer >> n;
	if (!buffer)
		throw std::runtime_error("Not an integer: " + s);
	if (buffer >> c)
	{
		switch (c)
		{
			case 'k':
			case 'K':
				return n * (binary_postfix ? 1024 : 1000);
			case 'm':
			case 'M':
				return n * (binary_postfix ? 1024 * 1024 : 1000 * 1000);
			default:
				throw std::runtime_error("Unknown multiplier: " + s);
		}
	}
	return n;
}
