#include "ByteBuffer.h"

void ByteBuffer::WriteWord(int16_t word)
{
	*reinterpret_cast<int16_t*>(pointer) = word;
	pointer += 2;
}

int16_t ByteBuffer::ReadWord()
{
	pointer += 2;
	return (pointer[-1] & 0xFF) << 8 | (pointer[-2] & 0xFF);
}

void ByteBuffer::Reset()
{
	pointer = data.data();
}
