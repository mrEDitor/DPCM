#include "Predictor.h"

void Predictor::Next(double actual_current)
{
	if (!memory.empty())
	{
		memory.front() = actual_current;
		double p = 0.0;
		for (size_t j = 0; j < coefficients.size(); j++)
		{
			p -= coefficients[j] * memory[j];
		}
		memory.push_front(p);
		memory.pop_back();
	}
}

double Predictor::Get()
{
	return memory.empty() ? 0.0 : memory.front();
}

std::vector<double> const& Predictor::CalculateCoefficients(int16_t block[], int size)
{
	std::vector<double> R(coefficients.size() + 1, 0.0);
	for (int i = 0; i <= int(coefficients.size()); i++)
	{
		for (int j = 0; j < size - i; j++)
		{
			R[i] += double(block[j]) * double(block[j + i]);
		}
	}
	std::vector<double> Ak(coefficients.size() + 1, 0.0);
	Ak[0] = 1.0;
	double Ek = R[0];
	if (Ek)
	{
		for (size_t k = 0; k < coefficients.size(); k++)
		{
			double lambda = 0.0;
			for (size_t j = 0; j <= k; j++)
			{
				lambda -= Ak[j] * R[k + 1 - j];
			}
			lambda /= Ek;
			for (size_t n = 0; n <= (k + 1) / 2; n++)
			{
				double temp = Ak[k + 1 - n] + lambda * Ak[n];
				Ak[n] = Ak[n] + lambda * Ak[k + 1 - n];
				Ak[k + 1 - n] = temp;
			}
			Ek *= 1.0 - lambda * lambda;
		}
	}
	coefficients.assign(++Ak.begin(), Ak.end());
	return coefficients;
}

void Predictor::UseCoefficients(double coeffs[])
{
	coefficients.assign(coeffs, coeffs + coefficients.size());
}
