#pragma once
#include <map>
#include <string>
#include <vector>
#include "utils.h"


class Parameters
{

public:
	Parameters() = delete;
	Parameters(int argc, cstring argv[], cstring config);
	Parameters(Parameters const&) = delete;
	~Parameters() = default;
	Parameters& operator = (Parameters const&) = delete;

	std::vector<std::string> const& GetArguments() const { return arguments; }
	std::map<std::string, std::string> const& GetOptions() const { return options; }

private:
	std::vector<std::string> arguments;
	std::map<std::string, std::string> options;

};
