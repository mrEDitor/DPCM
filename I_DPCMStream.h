#pragma once
#include "utils.h"
#include "ByteBuffer.h"


class I_DPCMStream
{

public:
	virtual ~I_DPCMStream() = default;
	virtual unsigned GetBitDepth() const = 0;
	virtual unsigned GetMemoryOrder() const = 0;
	virtual unsigned GetSampleRate() const = 0;
	virtual unsigned GetSamplesPerBlock() const = 0;
	virtual bool HasNextBlock() = 0;
	virtual size_t ReadBlock(double coeff[], byte block[]) = 0;
	virtual void WriteBlock(byte const *coeff, byte const *block, unsigned samples) = 0;
	virtual void Finalize() = 0;

};
