#pragma once
#include <condition_variable>
#include <mutex>
#include <thread>
#include <SFML/Network.hpp>
#include "I_DPCMStream.h"

class DPCMSocket :
	public I_DPCMStream
{

public:
	DPCMSocket() = delete;
	DPCMSocket(std::string const& address, unsigned port);
	DPCMSocket(unsigned port,
		unsigned bit_depth, unsigned sample_rate, unsigned memory_order, unsigned samples_per_block);
	DPCMSocket(DPCMSocket const&) = delete;
	virtual ~DPCMSocket() = default;
	DPCMSocket& operator = (DPCMSocket const&) = delete;

	void ListenForClient();

	bool HasNextBlock() override;
	size_t ReadBlock(double coeff[], byte block[]) override;
	void WriteBlock(byte const* coeff, byte const* block, unsigned samples) override;
	void Finalize() override {}

	unsigned GetSampleRate() const override { return header.sample_rate; }
	unsigned GetBitDepth() const override { return header.bit_depth; }
	unsigned GetMemoryOrder() const override { return header.memory_order; }
	unsigned GetSamplesPerBlock() const override { return header.samples_per_block; }
	std::string GetRemoteAddress() const { return socket.getRemoteAddress().toString(); }

private:
	enum { EMPTY, READY, FINALIZED } data_state;
	std::mutex data_mutex;
	std::condition_variable data_cond;
	sf::TcpSocket socket;
	sf::TcpListener listener;
	sf::Packet packet;
	struct
	{
		uint32_t sample_rate;
		uint16_t bit_depth;
		uint16_t memory_order;
		uint32_t samples_per_block;
	} header;
	unsigned port;
	std::thread listener_thread;

};
